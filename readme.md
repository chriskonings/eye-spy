#Light Up

##An Oculus Rift spy Game based in 1950s.

###Description:
Spying game with the use of an Oculus Rift as a main controller.
Set in 1950's to 1960's, with the use of period music, fashion and atmosphere.

You are moved in a location where you must observe and keep a look out for your target
who you will either indicate to be killed or stalked.

To detect or tell a fellow ally spy to assassinate you must 'light up'. This is when you
look at the target (using Oculus Rift) and choose to light a cigarette, this then indicates to a hidden sniper to shoot
the target (or to a ally spy to stalk out etc). The character model will raise his hands to light a cigarette and smoke particles will appear in front of your eyes in the Oculus Rift. The screen will change into slow motion as your cigarette points towards your target and they are shot or the screen fades to black as you complete a level.

You must also converse with some characters and can respond with a "yes" or "no" by shaking your head either 
up and down for "yes" or left and right for "no".

There will be a constant internal monologue from your character in order to help you understand what your character
is experiencing and give you hintsa s to how to play the game. For example: if you are running out of time your character will say "I have not got much time left".

You must find your target based on a mission file that you must read before entering the level. This will detail what characteristics that you must look out for, for exmaple: the target has a moustache and always wears red shoes etc.
If you do not read the mission file then you will have a hard time detecting your target.

If you make an error, for example stair down and nod at a random person, your suspicion level with rise and your charcter will say "I better keep my head down, people are starting to notice me".


###Idea:

- Russian Marilyn Monroe-type character, perhap prostitute that you sleep with, causing you to be late for mission.

- You cant speak Russian, the reason for the character not being able to talking to people and can only respond with nods.

- Set in USSR

- British spy, with british monologe.

- Constant internal monologue.

- If you drink alcohol throughout a level in a bar then your screen will get distorted and your character will say "If I drink any more I am going to pass out" to simulate running out of time.

- If you are to meet a fellow spy you must find them based on the identity brief you have been given. You will then see them, they will look at you and you must nod at them to invite them over to your table for them to brief you. If someone looks at you and you nod at them they will suspect you. Perhaps a prostitue will come over to your table if you nod at them?

###Locations:

- Cafe (Outside or inside)
- Bar (outside or inside)
- Swimming pool-side
- Aeroplane (on rails down the isle to justify time limit)
- The Ritz resteraunt

###Tech:

- Oculus Rift
- Use 'Make-Human' software to design multiple characters and crowds
- Rig skeleton movements in Mecanim and apply to multiple models