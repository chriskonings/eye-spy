﻿using UnityEngine;

public class RandomMatchMaker : MonoBehaviour
{

  //public GameObject cam1;
  //public GameObject cam2;

  void Start()
  {
    PhotonNetwork.ConnectUsingSettings("0.1");
  }

  void OnGUI()
  {
    GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
  }

  void OnJoinedLobby()
  {
    PhotonNetwork.JoinRandomRoom();
  }

  void OnPhotonRandomJoinFailed()
  {
    PhotonNetwork.CreateRoom(null);
  }

  void OnJoinedRoom()
  {
    // PhotonView pv = PhotonView.Get(this);

    GameObject player = PhotonNetwork.Instantiate("First Person Controller", gameObject.transform.position, Quaternion.identity, 0);

    // if(player.PhotonView.isMine)
    // {
      // initialize whatever you need to for the LOCAL player


      CharacterController controller = player.GetComponent<CharacterController>();
      controller.enabled = true;

      MouseLook mouseLook = player.GetComponent<MouseLook>();
      mouseLook.enabled = true;

      FPSInputController fpsinput = player.GetComponent<FPSInputController>();
      fpsinput.enabled = true;

      CharacterMotor characterMotor = player.GetComponent<CharacterMotor>();
      characterMotor.enabled = true;


      // assign player a random name
      // RandName = Random.Range(100, 200).ToString();
      // player.name = RandName;

      // player.GetComponentsInChildren("Main Camera");
      Transform cam = player.GetComponentInChildren<Transform>().Find("Main Camera");
      cam.GetComponent<Camera>().enabled = true;

      // GameObject.Find(RandName).Find("Main Camera").enabled = true;

    // }
    // else
    // {
    //   // destroy or disable things for REMOTE players

    // }



// Marco Polo Reference
// void OnJoinedRoom()
// {
//     GameObject monster = PhotonNetwork.Instantiate("monsterprefab", Vector3.zero, Quaternion.identity, 0);
//     CharacterControl controller = monster.GetComponent<CharacterControl>();
//     controller.enabled = true;
//     CharacterCamera camera = monster.GetComponent<CharacterCamera>();
//     camera.enabled = true;
// }






    // Oculus Rift Camera
    // if (LoadScript.riftEnabled) // check this
    // {
      //OVRPlayerController ovrcontroller = player.GetComponent<OVRPlayerController>();
      //ovrcontroller.enabled = true;

      //OVRCameraController ovrcameraleft = player.GetComponentsInChildren<OVRCameraController>();
      //ovrcameraleft.enabled = false;

      //cam1.active = true;
      //cam1.active = true;
    // }

  }
}


