﻿#pragma strict

public var character : GameObject;
public var characterSeated : GameObject;
public var isSeated : boolean;

function Awake () {

	if (isSeated) {
		Instantiate(characterSeated, Vector3(transform.position.x, 2.7, transform.position.z), Quaternion.identity);
	}
	else {
		Instantiate(character, Vector3(transform.position.x, 1, transform.position.z), Quaternion.identity);
	}

}