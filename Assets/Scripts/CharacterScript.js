﻿#pragma strict
//@script ExecuteInEditMode();

public var characterName : String;

public var isTarget : boolean;
public var isMale : boolean;
public var hat : boolean;
public var glasses : boolean;

public var hatObj : GameObject;
public var breastsObj : GameObject;
public var glassesObj : GameObject;

static var details = new Array();

private var killSwitch : boolean = false;
private var isRay : boolean = false;
private var fmin : int = 5;
private var fmax : int = 15;
private var hand : GameObject;

private var moving : boolean = false;

function Awake () {
  isMale = RandomBoolean();
  hat = RandomBoolean();
  glasses = RandomBoolean();

  characterName = Random.Range(1, 100).ToString();
  gameObject.name = characterName;
}

function Start () {
  if (hat) {
    hatObj.SetActive(true);
  }
  else {
    hatObj.SetActive(false);
  }

  if (glasses) {
    glassesObj.SetActive(true);
  }
  else {
    glassesObj.SetActive(false);
  }

  if (isMale) {
    breastsObj.SetActive(false);
  }
  else {
    breastsObj.SetActive(true);
  }

  if (gameObject.tag == "Target") { // isTarget
    details.Push(characterName);
    details.Push(isMale);
    details.Push(hat);
    details.Push(glasses);

    // @TODO disable while debugging multiplayer character prefabs.
    // hand = gameObject.FindWithTag("Hand");
    // hand.SendMessage("WriteMessage");
  }
  else {
    gameObject.particleSystem.Stop();
  }

}

function Update () {
//  moveCharacter();
}

function FixedUpdate () {
  KillForce();
}

function moveCharacter () {

  if (!moving) {

    moving = true;
    var rand = Random.Range(1, 4);

    if (rand == 1) {
      gameObject.rigidbody.AddForce(Vector3.forward * Random.Range(fmin, fmax));
    }

    if (rand == 2) {
      gameObject.rigidbody.AddForce(Vector3.right * Random.Range(fmin, fmax));
    }

    if (rand == 3) {
      gameObject.rigidbody.AddForce(Vector3.back * Random.Range(fmin, fmax));
    }

    if (rand == 4) {
      gameObject.rigidbody.AddForce(Vector3.left * Random.Range(fmin, fmax));
    }

    yield WaitForSeconds(3);
    moving = false;
    Invoke("moveCharacter", 1);
  }
}

function RandomBoolean() {
    if (Random.value >= 0.5) {
      return true;
    }
  return false;
}

function LookAtPlayer() {
  if (!isRay) {
    var charHead : GameObject;
    charHead = gameObject.Find(characterName + "/Head");
    charHead.GetComponent(SmoothLookAt).target = gameObject.FindWithTag("Player");

    isRay = true; // stop spamming of function

    yield WaitForSeconds(Random.Range(1, 5));

    charHead.GetComponent(SmoothLookAt).target = gameObject.FindWithTag("Prop");
    isRay = false;
  }
}

function SelectTarget() {
  if (gameObject.tag == "Target") {

    Debug.Log("Target Selected!");
    audio.Play();

    killSwitch = true;

    yield WaitForSeconds(2);

    Destroy(gameObject);
  }
  else {
    Debug.Log("Incorrect Target.");
//    Debug.Break();
//    Application.LoadLevel('gameOver');
  }
}

function KillForce () {
  if (killSwitch) {
    gameObject.rigidbody.AddForce(Vector3.forward * 10);
    yield WaitForSeconds(0.5);
    killSwitch = false;
  }
}

