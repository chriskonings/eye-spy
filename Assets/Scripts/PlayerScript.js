﻿#pragma strict

function Start () {
	Screen.lockCursor = true;
}

function Update(){

	var cam : Transform = Camera.main.transform;
	var ray = new Ray(cam.position, cam.forward);
	var hit : RaycastHit;

	Debug.DrawRay(gameObject.transform.position, gameObject.transform.forward, Color.green);

	if(Physics.Raycast (ray, hit, 500)){

		if (hit.collider.tag == "Character" || hit.collider.tag == "Target"){
			hit.collider.gameObject.SendMessage("LookAtPlayer");
		}

		if (hit.collider.tag == "Character"){
			if (Input.GetKeyDown("tab")){
				var currentScene : String = Application.loadedLevelName;
				Application.LoadLevel(currentScene);
			}
		}

		if (hit.collider.tag == "Target"){
			if (Input.GetKeyDown("tab")){
				hit.collider.gameObject.SendMessage("SelectTarget");
				// hit.collider.gameObject.SendMessage("LookAtPlayer");
				// @TODO ^ potentially refactor LookAtPlayer to be generic e.g. "LookAt", with argument. e.g. Target.
			}
		}
	}
}





