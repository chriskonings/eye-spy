﻿#pragma strict

private var message : String;
private var gender : String;
public var note : GameObject;

function WriteMessage () {
  // Debug.Log(CharacterScript.details.length);
  var characterName = CharacterScript.details[0];
  var isMale = CharacterScript.details[1];
  var hat = CharacterScript.details[2];
  var glasses = CharacterScript.details[3];

  if (isMale) {
    gender = "He";
  }
  else {
    gender = "She";
  }

  if (hat) {
    hat = " ";
  }
  else {
    hat = " not ";
  }

  if (glasses) {
    glasses = " ";
  }
  else {
    glasses = " not ";
  }

  message = gender + " is" + hat + "wearing a hat. " + "\n" + gender + " is" + glasses + "wearing a pair of glasses.";
  note = gameObject.Find("NoteText");
  note.GetComponent(TextMesh).text = message;

  Debug.Log(message);
}