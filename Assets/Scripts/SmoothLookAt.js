public var target : GameObject;
var damping = 6.0;
var smooth = true;

//@script AddComponentMenu("Camera-Control/Smooth Look At")

function Start () {
	target = gameObject.FindWithTag("Prop");
	
	if (rigidbody) {
		rigidbody.freezeRotation = true;        
	}
}

function LateUpdate () {
	if (target) {
		if (smooth)
		{
			var rotation = Quaternion.LookRotation(target.transform.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
		}
		else
		{
		    transform.LookAt(target.transform);
		}
	}
}
