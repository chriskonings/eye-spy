﻿#pragma strict

static var riftEnabled = false;

var chars : GameObject[];
// var charTarget : GameObject;

function Awake () {
	AssignTarget();
}


function Start () {
  // charTarget = gameObject.FindWithTag("Target");

}

function Update () {
	if (Input.GetKeyDown("space")) {
		var currentScene : String = Application.loadedLevelName;
		Application.LoadLevel(currentScene);
	}
}

function AssignTarget () {
	chars = gameObject.FindGameObjectsWithTag("Character");
	var charsInScene : int = chars.length;

	if (charsInScene > 0) {
		var randPerson : int = (Random.Range(1, charsInScene));

		chars[randPerson].tag = "Target";
		Debug.Log(chars[randPerson].name + " is now the target.");
	}
}