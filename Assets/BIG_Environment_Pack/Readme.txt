Thank you for purchasing the Big Environment Pack.

To get the same visual quality like in the preview videos, enable Deferred Lighting under: Edit/Project Settings/Player.

If you need high resolution texture versions for editing ( maximum 2048*2048 ) please contact me under: Philipp_Schmidt@Hotmail.com 

If you have any other questions about the BEP please don't hesitate to contact me.




Cheers,
Philipp


                                      